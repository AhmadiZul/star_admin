<!DOCTYPE html>
<html lang="en">
<head>
  <title>Product</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>Daftar Product</h2>
  <a href="<?php echo base_url() . "index.php/product/import" ?>">Import</a> 
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIM</th>
        <th>NIK</th>
        <th>Prodi</th>
        <th>Email</th>
        <th>Judul</th>
        <th>Dosen</th>
      </tr>
    </thead>
    <tbody>
     <?php $i = 1; foreach($mahasiswa as $value){ ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $value["nama"];?></td>
          <td><?php echo $value["nim"];?></td>
          <td><?php echo $value["nik"];?></td>
          <td><?php echo $value["prodi"];?></td>
          <td><?php echo $value["email"];?></td>
          <td><?php echo $value["proposal"];?></td>
          <td><?php echo $value["nama_d"];?></td>
        </tr>   
     <?php $i++; } ?>
    </tbody>
  </table>
</div>
</body>
</html>