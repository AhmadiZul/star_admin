<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_settings-panel.html -->
  <div class="theme-setting-wrapper">
    <div id="settings-trigger"><i class="ti-settings"></i></div>
    <div id="theme-settings" class="settings-panel">
      <i class="settings-close ti-close"></i>
      <p class="settings-heading">SIDEBAR SKINS</p>
      <div class="sidebar-bg-options selected" id="sidebar-light-theme">
        <div class="img-ss rounded-circle bg-light border me-3"></div>Light
      </div>
      <div class="sidebar-bg-options" id="sidebar-dark-theme">
        <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
      </div>
      <p class="settings-heading mt-2">HEADER SKINS</p>
      <div class="color-tiles mx-0 px-4">
        <div class="tiles success"></div>
        <div class="tiles warning"></div>
        <div class="tiles danger"></div>
        <div class="tiles info"></div>
        <div class="tiles light"></div>
        <div class="tiles primary"></div>
        <div class="tiles dark"></div>
        <div class="tiles default"></div>
      </div>
    </div>
  </div>
  
  <!-- partial -->
  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>dasboard">
          <i class="mdi mdi-grid-large menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>admin">
          <i class="mdi mdi-account-multiple menu-icon"></i>
          <span class="menu-title">Admin</span>
        </a>
      </li>
      <li class="nav-item nav-category">Menu Admin</li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Administrator</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin">Mahasiswa</a></li>
            <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/dosen">Dosen</a></li>
            <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Setting</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item nav-category">help</li>
      <li class="nav-item">
        <a class="nav-link" href="http://bootstrapdash.com/demo/star-admin2-free/docs/documentation.html">
          <i class="menu-icon mdi mdi-file-document"></i>
          <span class="menu-title">Documentation</span>
        </a>
      </li>
    </ul>
  </nav>
  <!-- partial -->
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-sm-12">
          <div class="home-tab">
            <div class="d-sm-flex align-items-center justify-content-between border-bottom">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="true">Data diri</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Data user</a>
                </li>
              </ul>
              <div>
                <div class="btn-wrapper">
                  <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a>
                  <a href="#" class="btn btn-otline-dark"><i class="icon-printer"></i> Print</a>
                  <button href="" class="btn btn-success text-white me-0" data-bs-toggle="modal" data-bs-target="#myModalTambah"><i class="mdi mdi-school"></i> Tambah</button>
                </div>
              </div>
            </div>
            <div class="tab-content tab-content-basic">
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?php
                    if ($this->session->flashdata('message_success') != '') {
                      echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                      echo '<strong>Success!! </strong>';
                      echo $this->session->flashdata('message_success');
                      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                      </button></div>';
                    }
                    ?>
                    <h4 class="card-title">Profil Saya</h4>
                    <p class="card-description">
                      My Profile
                    </p>
                    <form class="forms-sample" method="post" class="pt-3" action="<?php echo base_url(); ?>profil/upload_avatar" enctype="multipart/form-data">
                      <?php foreach ($mahasiswa as $row) : ?>
                        <div class="form-group row flex-grow">
                          <!-- <form action="" method="POST" enctype="multipart/form-data"> -->
                          <div class="col-6">
                            <h4 class="card-title">Upload Foto</h4>
                            <img class="img-lg rounded-circle center mb-2" src="<?= base_url($row->foto) ?>" alt="Profile image">
                            <div>
                              <label for="avatar">Pilih Gambar Avatar</label>
                              <input type="file" name="foto" id="foto" accept="image/png, image/jpeg, image/jpg, image/gif">
                            </div>
                            <div>
                              <button type="submit" name="save" class="button button-primary">Upload</button>
                            </div>
                          </div>
                          <!--  </form> -->
                          <div class="col-6">
                            <h4 class="card-title">Username & Password</h4>
                            <label for="exampleInputName1">Username</label>
                            <div class="input-group">
                              <input type="text" class="form-control" id="username" name="username" value="<?= $row->username ?>">
                            </div>
                            <label for="Nama" class="">Password</label>
                            <div class="input-group">
                              <input type="password" class="form-control" id="password-field" name="password" value="<?= $row->password ?>">
                              <span class="input-group-text"><a><i toggle="#password-field" class="mdi mdi-eye show-hide"></i></a></span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="<?= $row->nama ?>">
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Nomor Induk Mahasiswa</label>
                            <input type="text" class="form-control" id="nim" name="nim" value="<?= $row->nim ?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Nomor Induk Keluarga</label>
                            <input type="text" class="form-control" id="nik" name="nik" value="<?= $row->nik ?>">
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Agama</label>
                            <select class="form-control" id="provinsi" name="provinsi" value="<?= set_value('agama') ?>">
                              <label for="agama" class="">Agama</label>
                              <option value="">Pilih</option>
                              <?php
                              $selected = $row->agama;
                              foreach ($agama as $p) {
                                if ($selected == $p->id) { ?>
                                  <option value="<?php echo $p->id; ?>" selected><?php echo $p->nama_agama; ?></option>
                                <?php
                                } else { ?>
                                  <option value="<?php echo $p->id; ?>"><?php echo $p->nama_agama; ?></option>
                              <?php }
                              } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Tempat Lahir</label>
                            <input type="text" class="form-control" id="tempat" name="tempat" value="<?= $row->tempat ?>">
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?= $row->tanggal ?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Nomor handphone</label>
                            <input type="text" class="form-control" id="nomor" name="nomor" value="<?= $row->nomor ?>">
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Alamat Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= $row->email ?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" value="<?= $row->alamat ?>">
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Provinsi</label>
                            <select class="form-control" id="provinsi" name="provinsi" value="<?= set_value('provinsi') ?>">
                              <label for="provinsi" class="">Jenis Kelamin</label>
                              <option value="">Pilih</option>
                              <?php
                              $selected = $row->provinsi;
                              foreach ($provinces as $p) {
                                if ($selected == $p->id) { ?>
                                  <option value="<?php echo $p->id; ?>" selected><?php echo $p->name; ?></option>
                                <?php
                                } else { ?>
                                  <option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
                              <?php }
                              } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-6">
                            <label for="exampleInputName1">Kabupaten</label>
                            <select class="form-control" id="provinsi" name="provinsi" value="<?= set_value('provinsi') ?>">
                              <label for="provinsi" class="">Jenis Kelamin</label>
                              <option value="">Pilih</option>
                              <?php
                              $selected = $row->kabupaten;
                              foreach ($regencies as $r) {
                                if ($selected == $r->id) { ?>
                                  <option value="<?php echo $r->id; ?>" selected><?php echo $r->name; ?></option>
                                <?php
                                } else { ?>
                                  <option value="<?php echo $r->id; ?>"><?php echo $r->name; ?></option>
                              <?php }
                              } ?>
                            </select>
                          </div>
                          <div class="col-6">
                            <label for="exampleInputName1">Kecamatan</label>
                            <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
                          </div>
                        </div>
                      <?php endforeach; ?>
                      <button type="submit" class="btn btn-primary text-white">Submit</button>
                      <button class="btn btn-light">Cancel</button>
                    </form>
                  </div>
                </div>
              </div><!-- col-12 ends -->
            </div><!-- tabs-content ends -->
          </div><!-- home-tab ends -->
        </div><!-- col-sm-12 ends -->
      </div><!-- row ends -->
    </div><!-- content-wrapper ends -->
  </div><!-- main-panel ends -->
</div><!-- container-fluid ends -->






<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  $(".show-hide").click(function() {

    $(this).toggleClass("mdi-eye mdi-eye-off");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
</script>