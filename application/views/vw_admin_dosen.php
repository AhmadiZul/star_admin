<?php if ($this->session->userdata('access') == 'Admin') { ?>
  <div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_settings-panel.html -->
    <div class="theme-setting-wrapper">
      <div id="settings-trigger"><i class="ti-settings"></i></div>
      <div id="theme-settings" class="settings-panel">
        <i class="settings-close ti-close"></i>
        <p class="settings-heading">SIDEBAR SKINS</p>
        <div class="sidebar-bg-options selected" id="sidebar-light-theme">
          <div class="img-ss rounded-circle bg-light border me-3"></div>Light
        </div>
        <div class="sidebar-bg-options" id="sidebar-dark-theme">
          <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
        </div>
        <p class="settings-heading mt-2">HEADER SKIN</p>
        <div class="color-tiles mx-0 px-4">
          <div class="tiles success"></div>
          <div class="tiles warning"></div>
          <div class="tiles danger"></div>
          <div class="tiles info"></div>
          <div class="tiles light"></div>
          <div class="tiles primary"></div>
          <div class="tiles dark"></div>
          <div class="tiles default"></div>
        </div>
      </div>
    </div>
    <div id="right-sidebar" class="settings-panel">
      <i class="settings-close ti-close"></i>
      <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="todo-tab" data-bs-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="chats-tab" data-bs-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
        </li>
      </ul>
      <div class="tab-content" id="setting-content">
        <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
          <div class="add-items d-flex px-3 mb-0">
            <form class="form w-100">
              <div class="form-group d-flex">
                <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
              </div>
            </form>
          </div>
          <div class="list-wrapper px-3">
            <ul class="d-flex flex-column-reverse todo-list">
              <li>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="checkbox" type="checkbox">
                    Team review meeting at 3.00 PM
                  </label>
                </div>
                <i class="remove ti-close"></i>
              </li>
              <li>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="checkbox" type="checkbox">
                    Prepare for presentation
                  </label>
                </div>
                <i class="remove ti-close"></i>
              </li>
              <li>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="checkbox" type="checkbox">
                    Resolve all the low priority tickets due today
                  </label>
                </div>
                <i class="remove ti-close"></i>
              </li>
              <li class="completed">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="checkbox" type="checkbox" checked>
                    Schedule meeting for next week
                  </label>
                </div>
                <i class="remove ti-close"></i>
              </li>
              <li class="completed">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="checkbox" type="checkbox" checked>
                    Project review
                  </label>
                </div>
                <i class="remove ti-close"></i>
              </li>
            </ul>
          </div>
          <h4 class="px-3 text-muted mt-5 fw-light mb-0">Events</h4>
          <div class="events pt-4 px-3">
            <div class="wrapper d-flex mb-2">
              <i class="ti-control-record text-primary me-2"></i>
              <span>Feb 11 2018</span>
            </div>
            <p class="mb-0 font-weight-thin text-gray">Creating component page build a js</p>
            <p class="text-gray mb-0">The total number of sessions</p>
          </div>
          <div class="events pt-4 px-3">
            <div class="wrapper d-flex mb-2">
              <i class="ti-control-record text-primary me-2"></i>
              <span>Feb 7 2018</span>
            </div>
            <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
            <p class="text-gray mb-0 ">Call Sarah Graves</p>
          </div>
        </div>
        <!-- To do section tab ends -->
        <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
          <div class="d-flex align-items-center justify-content-between border-bottom">
            <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
            <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 fw-normal">See All</small>
          </div>
          <ul class="chat-list">
            <li class="list active">
              <div class="profile"><img src="images/faces/face1.jpg" alt="image"><span class="online"></span></div>
              <div class="info">
                <p>Thomas Douglas</p>
                <p>Available</p>
              </div>
              <small class="text-muted my-auto">19 min</small>
            </li>
            <li class="list">
              <div class="profile"><img src="images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
              <div class="info">
                <div class="wrapper d-flex">
                  <p>Catherine</p>
                </div>
                <p>Away</p>
              </div>
              <div class="badge badge-success badge-pill my-auto mx-2">4</div>
              <small class="text-muted my-auto">23 min</small>
            </li>
            <li class="list">
              <div class="profile"><img src="images/faces/face3.jpg" alt="image"><span class="online"></span></div>
              <div class="info">
                <p>Daniel Russell</p>
                <p>Available</p>
              </div>
              <small class="text-muted my-auto">14 min</small>
            </li>
            <li class="list">
              <div class="profile"><img src="images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
              <div class="info">
                <p>James Richardson</p>
                <p>Away</p>
              </div>
              <small class="text-muted my-auto">2 min</small>
            </li>
            <li class="list">
              <div class="profile"><img src="images/faces/face5.jpg" alt="image"><span class="online"></span></div>
              <div class="info">
                <p>Madeline Kennedy</p>
                <p>Available</p>
              </div>
              <small class="text-muted my-auto">5 min</small>
            </li>
            <li class="list">
              <div class="profile"><img src="images/faces/face6.jpg" alt="image"><span class="online"></span></div>
              <div class="info">
                <p>Sarah Graves</p>
                <p>Available</p>
              </div>
              <small class="text-muted my-auto">47 min</small>
            </li>
          </ul>
        </div>
        <!-- chat tab ends -->
      </div>
    </div>
    <!-- partial -->
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>dasboard">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Dashboard</span>
          </a>
        </li>
        <li class="nav-item nav-category">Menu Admin</li>
        <li class="nav-item">
          <a class="nav-link" data-bs-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
            <i class="menu-icon mdi mdi-floor-plan"></i>
            <span class="menu-title">Administrator</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="ui-basic">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin">Mahasiswa</a></li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>admin/dosen">Dosen</a></li>
              <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Setting</a></li>
            </ul>
          </div>
        </li>

        <li class="nav-item nav-category">help</li>
        <li class="nav-item">
          <a class="nav-link" href="http://bootstrapdash.com/demo/star-admin2-free/docs/documentation.html">
            <i class="menu-icon mdi mdi-file-document"></i>
            <span class="menu-title">Documentation</span>
          </a>
        </li>
      </ul>
    </nav>
    <!-- partial -->
    <div class="main-panel">
      <div class="content-wrapper">
        <div class="row">
          <div class="col-sm-12">
            <div class="home-tab">
              <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Dosen</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="true">Data diri</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Data user</a>
                  </li>
                </ul>
                <div>
                  <div class="btn-wrapper">
                    <button href="" class="btn btn-otline-dark align-items-center" data-bs-toggle="modal" data-bs-target="#ModalTambah"><i class="icon-share"></i> Import</button>
                    <a href="<?php echo base_url(); ?>admin/excel_dosen" class="btn btn-otline-dark"><i class="icon-printer"></i> Export</a>
                    <button href="" class="btn btn-success text-white me-0" data-bs-toggle="modal" data-bs-target="#myModalTambah"><i class="mdi mdi-school"></i> Tambah</button>
                  </div>
                </div>
              </div>
              <div class="tab-content tab-content-basic">
                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <?php
                                if ($this->session->flashdata('sukses') != '') {
                                  echo '<div class="alert alert-success alert-dismissible fade show" role="alert">';
                                  echo '<strong>Success!! </strong>';
                                  echo $this->session->flashdata('sukses');
                                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                              </button></div>';
                                }
                                ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Tabel Dosen</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Dosen Universitas Sebelas Maret</h5>
                                </div>
                                <!-- <div id="performance-line-legend"></div> -->
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="item-list">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>NO</th>
                                      <th>NAMA</th>
                                      <th>NIP</th>
                                      <th>PRODI</th>
                                      <th>JENIS KELAMIN</th>
                                      <th>PENDIDIKAN TERAKHIR</th>
                                    </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="audiences" role="tabpanel" aria-labelledby="audiences">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <!-- Menampilkan flashh data (pesan saat data berhasil disimpan)-->
                                <?php if ($this->session->flashdata('message')) :
                                  echo $this->session->flashdata('message');
                                endif; ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Halaman Dosen</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Dosen Universitas Sebelas Maret</h5>
                                </div>
                                <!-- <div id="performance-line-legend"></div> -->
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>Nama</th>
                                      <th>Email</th>
                                      <th>Proposal</th>
                                      <th>Penilaian</th>
                                    </tr>
                                  </thead>
                                  <tbody>

                                    <!-- <tr> -->

                                    <!--  <td> -->
                                    <!-- <button class="badge badge-warning" type="button" href="<?= site_url('mahasiswa/edit' . $row->IdMhsw) ?>">Edit</button>
                                            <button class="badge badge-danger item-delete" type="button" href="javascript:void(0);" data="<?= $row->IdMhsw ?>">Hapus</button> -->

                                    <!-- </td> -->
                                    <!-- </tr> -->


                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="demographics" role="tabpanel" aria-labelledby="demographics">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <!-- Menampilkan flashh data (pesan saat data berhasil disimpan)-->
                                <?php if ($this->session->flashdata('message')) :
                                  echo $this->session->flashdata('message');
                                endif; ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Tambah Admin</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Admin Universitas Sebelas Maret</h5>
                                </div>
                                <!-- <div id="performance-line-legend"></div> -->
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>Nama</th>
                                      <th>NIM</th>
                                      <th>Alamat</th>
                                      <th>Agama</th>
                                      <th>No Hp</th>
                                      <th>Email</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- tabs-content ends -->
            </div>
          </div>
        </div>
      </div><!-- content-wrapper ends -->
    </div><!-- main-panel ends -->
  </div><!-- container-fluid ends -->

  <div class="modal fade" id="myModalEdit" aria-labelledby="myModalEditLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit</h5>
          <i class="settings-close ti-close"></i>
        </div>
        <form id="FrmEditMahasiswa" method="post">
          <div class="modal-body">
            <div class="card-body">
              <div class="form-group row">
                <div class="col-sm-5">
                  <label for="Nama" class="">Nama</label>
                  <input type="hidden" class="form-control" id="IdMhsw" name="IdMhsw" value="">
                  <input type="text" class="form-control" id="Nama" name="Nama" value="">
                </div>
                <div class="col-sm-5">
                  <label for="Nama" class="">Jenis Kelamin</label>
                  <select class="form-control" id="JenisKelamin" name="JenisKelamin">
                    <label for="JenisKelamin" class="">Jenis Kelamin</label>
                    <option value="">Pilih</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-5">
                  <label for="Alamat" class="">Alamat</label>
                  <textarea class="form-control" id="Alamat" name="Alamat" rows="3"></textarea>
                </div>
                <div class="col-sm-5">
                  <label for="Agama" class="">Agama</label>
                  <select class="form-control" id="Agama" name="Agama">
                    <option value="">Pilih</option>
                    <option value="Islam">Islam</option>
                    <option value="Protestan">Protestan</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Buddha">Buddha</option>
                    <option value="Khonghucu">Khonghucu</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label for="NoHp" class="">No Hp</label>
                  <input type="text" class="form-control" id="NoHp" name="NoHp" value="">
                </div>
                <div class="col-sm-6">
                  <label for="Email" class="">Email</label>
                  <input type="text" class="form-control" id="Email" name="Email" value="">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10 offset-md-2">
                  <button type="submit" class="btn btn-primary text-white">Simpan</button>
                  <a class="btn btn-secondary" href="javascript:history.back()">Kembali</a>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- modal tambah dosen -->
  <div class="modal fade" id="myModalTambah">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Dosen</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="FrmAddMahasiswa" method="post">
            <div class="form-group row">
              <div class="col-sm-6">
                <label for="Nama" class="">Nama</label>
                <input type="hidden" class="form-control" id="id" name="id" value="">
                <input type="text" class="form-control" id="nama" name="nama" value="">

              </div>
              <div class="col-sm-6">
                <label for="Nama" class="">email</label>
                <input type="text" class="form-control" id="email" name="email" value="">
                <div class="invalid-feedback" for="email"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <label for="dosen" class="">jenis Kelamin</label>
                <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="">
                <div class="invalid-feedback" for="jenis_kelamin"></div>
              </div>
              <div class="col-sm-6">
                <label for="Nama" class="">Nomor Induk Pendidik</label>
                <input type="text" class="form-control" id="nip" name="nip" value="">
                <div class="invalid-feedback" for="nip"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <label for="dosen" class="">Program Studi</label>
                <input type="text" class="form-control" id="prodi" name="prodi" value="">
                <div class="invalid-feedback" for="prodi"></div>
              </div>
              <div class="col-sm-6">
                <label for="dosen" class="">Pendidikan Terakhir</label>
                <input type="text" class="form-control" id="pendidikan_terakhir" name="pendidikan_terakhir" value="">
                <div class="invalid-feedback" for="pendidikan_terakhir"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <label for="Nama" class="">Password</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="password-field" name="password" value="">
                  <span class="input-group-text"><a><i toggle="#password-field" class="mdi mdi-eye show-hide"></i></a></span>
                </div>
                <div class="invalid-feedback" for="proposal"></div>
              </div>
              <div class="col-sm-6">
                <label for="Nama" class="">Confirm Password</label>
                <div class="input-group">
                  <input type="password" class="form-control" id="password-field2" name="confirm_password" value="">
                  <span class="input-group-text"><a><i toggle="#password-field2" class="mdi mdi-eye show-hide"></i></a></span>
                </div>
                <div class="invalid-feedback" for="proposal"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary btn-sm text-white">Simpan</button>
            <a type="submit" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Kembali</a>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal-tambah-dosen ends -->

  <!-- modal import  -->
  <div class="modal fade" id="ModalTambah">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Import Excel</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <?php echo form_open('Admin/import_excel_dosen', ['enctype' => 'multipart/form-data']) ?>
          <div class="sub-result"></div>
          <div class="form-group">
            <label class="control-label">Choose File <small class="text-danger">*</small></label>
            <input type="file" class="form-control form-control-sm" id="nama" name="file_excel" required>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary btn-sm text-white">Simpan</button>
            <a type="submit" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Kembali</a>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- modal import ends -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
    $(".show-hide").click(function() {

      $(this).toggleClass("mdi-eye mdi-eye-off");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  </script>
  <script type="text/javascript">
    //menampilkan data ketabel dengan plugin datatables
    var tabel;
    $(document).ready(function() {
      tabelmhs = $('#item-list').DataTable({
        "ajax": {
          url: "<?= base_url('Admin/get_admin_dosen') ?>",
          type: 'GET',
          columns: [{
              data: 'no'
            },
            {
              data: 'nama'
            },
            {
              data: 'nip'
            },
            {
              data: 'aksi'
            },
            {
              data: 'aksi'
            },
            {
              data: 'aksi'
            }
          ],

        },
      });
    });

    var tabel;
    $(document).ready(function() {
      tabel = $('#item-dosen').DataTable({
        "ajax": {
          url: "<?= base_url('get_itums') ?>",
          type: 'GET',
          columns: [{
              data: 'nama'
            },
            {
              data: 'email'
            },
            {
              data: 'proposal'
            },
            {
              data: 'Aksi'
            }
          ],

        },
      });
    });

    //menampilkan modal dialog saat tombol hapus ditekan
    /* $('#tableMahasiswa').DataTable(); */
    $('#FrmAddMahasiswa').submit(function(e) {
      e.preventDefault();
      var form = $(this);
      $('#FrmAddMahasiswa').find('input, select').removeClass('is-invalid');
      $('input').parent().removeClass('is-invalid');
      $.ajax({
        url: '<?php echo base_url('dosen_tambah') ?>',
        type: 'POST',
        dataType: "JSON",
        data: form.serialize(),
        error: function() {
          alert('Something is wrong');
        },
        success: function(data) {
          if (data.success) {
            $('#FrmAddMahasiswa')[0].reset();
            $('#myModalTambah').modal('hide');
            tabelmhs.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timerProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: data.message
            });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);
                $('[name="' + key + '"]').parent().addClass('is-invalid');
                $('[name="' + key + '"]').addClass('is-invalid');
              }
            })
          }

        }
      });
    });
    //menampilkan modal dialog saat tombol hapus ditekan
    $('#modal_verif').on('show.bs.modal', function(event) {
      const button = event.relatedTarget; // Button that triggered the modal
      var nama = button.getAttribute('data-bs-nama');
      var proposal = button.getAttribute('data-bs-proposal');
      var id = button.getAttribute('data-bs-id');
      console.log(id);
      $('#nama_verif').val(nama);
      $('#judul_verif').val(proposal);
      $('#id').val(id);
      $('#form_verif').attr('action', '<?= base_url("dosen/verif") ?>');
    });

    $('#form_verif').submit(function(e) {
      e.preventDefault();
      var form = $(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        dataType: "JSON",
        data: form.serialize(),
        error: function() {
          alert('Something is wrong');
        },
        success: function(data) {
          if (data.success) {
            $('#modal_verif').modal('hide');
            tabel.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timerProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: data.message
            });
          } else alert('Something is GJ');

        }
      });
    });
  </script>

<?php }; ?>