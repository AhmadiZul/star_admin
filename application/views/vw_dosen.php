<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_settings-panel.html -->
  <div class="theme-setting-wrapper">
    <div id="settings-trigger"><i class="ti-settings"></i></div>
    <div id="theme-settings" class="settings-panel">
      <i class="settings-close ti-close"></i>
      <p class="settings-heading">SIDEBAR SKINS</p>
      <div class="sidebar-bg-options selected" id="sidebar-light-theme">
        <div class="img-ss rounded-circle bg-light border me-3"></div>Light
      </div>
      <div class="sidebar-bg-options" id="sidebar-dark-theme">
        <div class="img-ss rounded-circle bg-dark border me-3"></div>Dark
      </div>
      <p class="settings-heading mt-2">HEADER SKINS</p>
      <div class="color-tiles mx-0 px-4">
        <div class="tiles success"></div>
        <div class="tiles warning"></div>
        <div class="tiles danger"></div>
        <div class="tiles info"></div>
        <div class="tiles light"></div>
        <div class="tiles primary"></div>
        <div class="tiles dark"></div>
        <div class="tiles default"></div>
      </div>
    </div>
  </div>

  <!-- partial -->
  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>dasboard">
          <i class="mdi mdi-grid-large menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <?php if ($this->session->userdata('access') == 'Mahasiswa') { ?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>mahasiswa">
            <i class="mdi mdi-account-multiple menu-icon"></i>
            <span class="menu-title">Mahasiswa</span>
          </a>
        </li>
      <?php }; ?>
      <?php if ($this->session->userdata('access') == 'Dosen') { ?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>mahasiswa">
            <i class="mdi mdi-account-multiple menu-icon"></i>
            <span class="menu-title">Dosen</span>
          </a>
        </li>
      <?php }; ?>
      <?php if ($this->session->userdata('access') == 'Admin') { ?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>mahasiswa">
            <i class="mdi mdi-account-multiple menu-icon"></i>
            <span class="menu-title">Admin</span>
          </a>
        </li>
      <?php }; ?>
      <li class="nav-item nav-category">help</li>
      <li class="nav-item">
        <a class="nav-link" href="http://bootstrapdash.com/demo/star-admin2-free/docs/documentation.html">
          <i class="menu-icon mdi mdi-file-document"></i>
          <span class="menu-title">Documentation</span>
        </a>
      </li>
    </ul>
  </nav>
  <!-- partial -->
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-sm-12">
          <div class="home-tab">
            <div class="d-sm-flex align-items-center justify-content-between border-bottom">
              <ul class="nav nav-tabs" role="tablist">
                <?php if ($this->session->userdata('access') == 'Mahasiswa') { ?>
                  <li class="nav-item">
                    <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Mahasiswa</a>
                  </li>
                <?php }; ?>
                <?php if ($this->session->userdata('access') == 'Dosen') { ?>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="true">Dosen</a>
                  </li>
                <?php }; ?>
                <?php if ($this->session->userdata('access') == 'Admin') { ?>
                  <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#demographics" role="tab" aria-selected="false">Admin</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="true">Dosen</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Mahasiswa</a>
                  </li>
                <?php }; ?>
              </ul>
              <div>
                <div class="btn-wrapper">
                  <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a>
                  <a href="<?php echo base_url(); ?>mahasiswa/pdf" class="btn btn-otline-dark"><i class="icon-printer"></i> Print</a>
                  <?php if ($this->session->userdata('access') == 'Mahasiswa') { ?>
                    <button href="" class="btn btn-success text-white me-0" data-bs-toggle="modal" data-bs-target="#myModalTambah"><i class="mdi mdi-school"></i> Tambah</button>
                  <?php }; ?>
                </div>
              </div>
            </div>
            <div class="tab-content tab-content-basic">
              <?php if ($this->session->userdata('access') == 'Mahasiswa') { ?>
                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <!-- Menampilkan flashh data (pesan saat data berhasil disimpan)-->
                                <?php if ($this->session->flashdata('message')) :
                                  echo $this->session->flashdata('message');
                                endif; ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Pengajuan Proposal</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Mahasiswa Universitas Sebelas Maret</h5>
                                </div>
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="item-list">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Email</th>
                                      <th>Judul Proposal</th>
                                      <th>Dosen</th>
                                      <th>Status</th>
                                      <th>Cetak</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              <?php }; ?>
              <?php if ($this->session->userdata('access') == 'Dosen') { ?>
                <div class="tab-pane fade show active" id="audiences" role="tabpanel" aria-labelledby="audiences">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <!-- Menampilkan flashh data (pesan saat data berhasil disimpan)-->
                                <?php if ($this->session->flashdata('message')) :
                                  echo $this->session->flashdata('message');
                                endif; ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Halaman Dosen</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Dosen Universitas Sebelas Maret</h5>
                                </div>
                                <!-- <div id="performance-line-legend"></div> -->
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="item-dosen">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Email</th>
                                      <th>Proposal</th>
                                      <th>Penilaian</th>
                                    </tr>
                                  </thead>
                                  <tbody>


                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              <?php }; ?>
              <?php if ($this->session->userdata('access') == 'Admin') { ?>
                <div class="tab-pane fade show active" id="demographics" role="tabpanel" aria-labelledby="demographics">
                  <div class="row">
                    <div class="col-lg-12 d-flex flex-column">
                      <div class="row flex-grow">
                        <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                          <div class="card card-rounded">
                            <div class="card-body">
                              <div mb-2>
                                <!-- Menampilkan flashh data (pesan saat data berhasil disimpan)-->
                                <?php if ($this->session->flashdata('message')) :
                                  echo $this->session->flashdata('message');
                                endif; ?>
                              </div>
                              <div class="d-sm-flex justify-content-between align-items-start">
                                <div>
                                  <h4 class="card-title card-title-dash">Tambah Admin</h4>
                                  <h5 class="card-subtitle card-subtitle-dash">Admin Universitas Sebelas Maret</h5>
                                </div>
                                <!-- <div id="performance-line-legend"></div> -->
                              </div>

                              <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="">
                                  <thead class="bg-light">
                                    <tr>
                                      <th>Nama</th>
                                      <th>NIM</th>
                                      <th>Alamat</th>
                                      <th>Agama</th>
                                      <th>No Hp</th>
                                      <th>Email</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                      <th></th>
                                    </tr>


                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              <?php }; ?>
            </div><!-- tabs-content ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->

    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>

<!-- modal verifikasi -->
<section id="form-and-scrolling-components">
  <div class="row">
    <div class="col-md-6 col-12">
      <div class="form-group">
        <div class="modal fade text-left" id="modal_verif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">Persetujuan Judul Proposal</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form action="" id="form_verif">
                <div class="modal-body">
                  <label>Judul: </label>
                  <div class="form-group">
                    <input type="text" id="judul_verif" class="form-control" disabled />
                    <input type="hidden" id="id" name="id" class="form-control" />
                  </div>
                  <label>Nama Mahasiswa: </label>
                  <div class="form-group">
                    <input type="text" name="nama_verif" id="nama_verif" class="form-control" disabled />
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                  </button>
                  <button type="submit" class="btn btn-primary ml-1">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Verifikasi</span>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end modal verifikasi -->
<!-- modal tambah proposal -->
<div class="modal fade" id="myModalTambah" aria-labelledby="myModalTambahLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <form id="FrmAddMahasiswa" method="post">
          <div class="form-group row">
            <div class="col-sm-6">
              <label for="Nama" class="">Judul</label>
              <input type="hidden" class="form-control" id="id" name="id" value="">
              <input type="text" class="form-control" id="proposal" name="proposal" value="">
              <div class="invalid-feedback" for="proposal"></div>
            </div>
            <div class="col-sm-6">
              <label for="dosen" class="">Dosen</label>
              <select class="form-control" id="dosen" name="dosen">
                <option value="">Pilih</option>
                <?php foreach ($dosen as $row) : ?>
                  <option value="<?php echo $row->id; ?>" <?php if (isset($_POST['dosen']) && $_POST['dosen'] == $row->id) echo 'selected="selected"'; ?>><?php echo $row->nama_d; ?></option>
                <?php endforeach; ?>
              </select>
              <div class="invalid-feedback" for="dosen"></div>
            </div>

          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
          <i class="bx bx-x d-block d-sm-none"></i>
          <span class="d-none d-sm-block">Close</span>
        </button>
        <button type="submit" class="btn btn-primary ml-1">
          <i class="bx bx-check d-block d-sm-none"></i>
          <span class="d-none d-sm-block">Tambah</span>
        </button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- end modal tambah proposal -->


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  //menampilkan data ketabel dengan plugin datatables
  var tabel;
  $(document).ready(function() {
    tabelmhs = $('#item-list').DataTable({
      "ajax": {
        url: "<?= base_url('table_mahasiswa') ?>",
        type: 'GET',
        columns: [{
            data: 'no'
          },
          {
            data: 'nama'
          },
          {
            data: 'email'
          },
          {
            data: 'proposal'
          },
          {
            data: 'dosen'
          },
          {
            data: 'Aksi'
          },
          {
            data: 'cetak'
          }
        ],

      },
    });
  });

  var tabel;
  $(document).ready(function() {
    tabel = $('#item-dosen').DataTable({
      "ajax": {
        url: "<?= base_url('table_dosen') ?>",
        type: 'GET',
        columns: [{
            data: 'no'
          },
          {
            data: 'nama'
          },
          {
            data: 'email'
          },
          {
            data: 'proposal'
          },
          {
            data: 'Aksi'
          }
        ],

      },
    });
  });


  //menampilkan modal dialog saat tombol hapus ditekan

  $('#FrmAddMahasiswa').submit(function(e) {
    e.preventDefault();
    var form = $(this);
    $('#FrmAddMahasiswa').find('input, select').removeClass('is-invalid');
    $('input').parent().removeClass('is-invalid');
    $.ajax({
      url: '<?php echo base_url('tambah') ?>',
      type: 'POST',
      dataType: "JSON",
      data: form.serialize(),
      error: function() {
        alert('Something is wrong');
      },
      success: function(data) {
        if (data.success) {
          $('#FrmAddMahasiswa')[0].reset();
          $('#myModalTambah').modal('hide');
          tabelmhs.ajax.reload();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })

          Toast.fire({
            icon: 'success',
            title: data.message
          });
        } else {
          $.each(data.message, function(key, val) {
            if (val != "") {
              $('.invalid-feedback[for="' + key + '"]').html(val);
              $('[name="' + key + '"]').parent().addClass('is-invalid');
              $('[name="' + key + '"]').addClass('is-invalid');
            }
          })
        }

      }
    });
  });
  //menampilkan modal dialog saat tombol hapus ditekan
  $('#modal_verif').on('show.bs.modal', function(event) {
    const button = event.relatedTarget; // Button that triggered the modal
    var nama = button.getAttribute('data-bs-nama');
    var proposal = button.getAttribute('data-bs-proposal');
    var id = button.getAttribute('data-bs-id');
    console.log(id);
    $('#nama_verif').val(nama);
    $('#judul_verif').val(proposal);
    $('#id').val(id);
    $('#form_verif').attr('action', '<?= base_url("dosen/verif") ?>');
  });

  $('#form_verif').submit(function(e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      dataType: "JSON",
      data: form.serialize(),
      error: function() {
        alert('Something is wrong');
      },
      success: function(data) {
        if (data.success) {
          $('#modal_verif').modal('hide');
          tabel.ajax.reload();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })

          Toast.fire({
            icon: 'success',
            title: data.message
          });
        } else alert('Something is GJ');

      }
    });
  });
</script>