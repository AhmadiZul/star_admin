<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{
    function __construct()
  {
    parent::__construct();
    $this->load->model("Mahasiswa_model");
    $this->load->model("M_regis");
    if ($this->session->userdata('logged') != TRUE) {
      $url = base_url('login');
      redirect($url);
    };
  }

  public function index ()
  {
    $data['dosen'] = $this->Mahasiswa_model->dosen()->result();
    $foto['mahasiswa'] = $this->M_regis->mahasiswa();
    $this->load->view('tpls/sidebar', $foto);
    $this->load->view('vw_mahasiswa', $data);
    $this->load->view('tpls/footer');
  }

  public function get_dosen()
   {
      $query = $this->Mahasiswa_model->semua();

      $data = [];
      $btn_verif = "";

      foreach($query as $p => $r) {
         if($r->status === '1'){
            $btn_verif = '<span class="badge bg-success">Terverifikasi</span>';
        }else{
            $btn_verif = '<button type="button" class="badge bg-warning" data-bs-id ="'.$r->id.'" data-bs-nama ="'.$r->nama.'" data-bs-proposal="'.$r->proposal.'" data-bs-toggle="modal" data-bs-target="#modal_verif">Belum Terverifikasi</button>';
        }
         $data[] = array(
            $p+1,
            $r->nama,
            $r->email,
            '<strong>'.$r->proposal.'</strong>',
           $btn_verif
       );
      }
      $result = array(
                 "data" => $data
            );

      echo json_encode($result);
   }

   public function verif()
   {
       $id = $this->input->post("id");
       
       $data = array(
           'status' => '1',
       );
       $this->Mahasiswa_model->change_status($id, $data);
     echo json_encode(['success' => true, 'message' => 'Judul Proposal Sudah di Verifikasi !']);
   }
}

?>