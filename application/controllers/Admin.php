<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Admin_model");
    if ($this->session->userdata('logged') != TRUE) {
      $url = base_url('login');
      redirect($url);
    };
  }


  public function index()
  {
    $data["title"] = "List Data Mahasiswa";
    $data["data_mahasiswa"] = $this->Admin_model->getAll();
    $this->load->view('tpls/sidebar');
    $this->load->view('vw_admin', $data);
    $this->load->view('tpls/footer');
  }

  public function dosen()
  {
    $data["data_mahasiswa"] = $this->Admin_model->dosen();
    $this->load->view('tpls/sidebar');
    $this->load->view('vw_admin_dosen', $data);
    $this->load->view('tpls/footer');
  }

  public function excel()
  {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = [
      'font' => ['bold' => true], // Set font nya jadi bold
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = [
      'alignment' => [
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    $sheet->setCellValue('A1', "DATA MAHASISWA UNIVERSITAS SEBELAS MARET"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $sheet->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
    $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1
    // Buat header tabel nya pada baris ke 3
    $sheet->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $sheet->setCellValue('B3', "NIM"); // Set kolom B3 dengan tulisan "NIS"
    $sheet->setCellValue('C3', "NAMA"); // Set kolom C3 dengan tulisan "NAMA"
    $sheet->setCellValue('D3', "EMAIL"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $sheet->setCellValue('E3', "PRODI");
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $sheet->getStyle('A3')->applyFromArray($style_col);
    $sheet->getStyle('B3')->applyFromArray($style_col);
    $sheet->getStyle('C3')->applyFromArray($style_col);
    $sheet->getStyle('D3')->applyFromArray($style_col);
    $sheet->getStyle('E3')->applyFromArray($style_col);
    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
    $mahasiswa = $this->Admin_model->admin();
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach ($mahasiswa as $data) { // Lakukan looping pada variabel siswa
      $sheet->setCellValue('A' . $numrow, $no);
      $sheet->setCellValue('B' . $numrow, $data->nim);
      $sheet->setCellValue('C' . $numrow, $data->nama);
      $sheet->setCellValue('D' . $numrow, $data->nama_agama);
      $sheet->setCellValue('E' . $numrow, $data->nama_prodi);

      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $sheet->getStyle('A' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('B' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('C' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('D' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('E' . $numrow)->applyFromArray($style_row);

      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $sheet->getColumnDimension('E')->setWidth(30); // Set width kolom E

    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $sheet->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $sheet->setTitle("Laporan Data Siswa");
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Mahasiswa.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');
  }

  public function excel_dosen()
  {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = [
      'font' => ['bold' => true], // Set font nya jadi bold
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = [
      'alignment' => [
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    $sheet->setCellValue('A1', "DATA DOSEN UNIVERSITAS SEBELAS MARET"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $sheet->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
    $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1
    // Buat header tabel nya pada baris ke 3
    $sheet->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $sheet->setCellValue('B3', "NIP"); // Set kolom B3 dengan tulisan "NIS"
    $sheet->setCellValue('C3', "NAMA"); // Set kolom C3 dengan tulisan "NAMA"
    $sheet->setCellValue('D3', "PRODI"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $sheet->setCellValue('E3', "JENIS KELAMIN");
    $sheet->setCellValue('F3', "PENDIDIKAN TERAKHIR");
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $sheet->getStyle('A3')->applyFromArray($style_col);
    $sheet->getStyle('B3')->applyFromArray($style_col);
    $sheet->getStyle('C3')->applyFromArray($style_col);
    $sheet->getStyle('D3')->applyFromArray($style_col);
    $sheet->getStyle('E3')->applyFromArray($style_col);
    $sheet->getStyle('F3')->applyFromArray($style_col);
    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
    $mahasiswa = $this->Admin_model->dosen();
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach ($mahasiswa as $data) { // Lakukan looping pada variabel siswa
      $sheet->setCellValue('A' . $numrow, $no);
      $sheet->setCellValue('B' . $numrow, $data->nip);
      $sheet->setCellValue('C' . $numrow, $data->nama_d);
      $sheet->setCellValue('D' . $numrow, $data->prodi);
      $sheet->setCellValue('E' . $numrow, $data->jenis_kelamin);
      $sheet->setCellValue('F' . $numrow, $data->pendidikan_terakhir);

      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $sheet->getStyle('A' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('B' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('C' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('D' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('E' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('F' . $numrow)->applyFromArray($style_row);

      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $sheet->getColumnDimension('E')->setWidth(30); // Set width kolom E
    $sheet->getColumnDimension('F')->setWidth(35);

    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $sheet->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $sheet->setTitle("Laporan Data Dosen");
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Dosen.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');
  }



  public function import_excel_dosen()
  {
    $upload_file = $_FILES["file_excel"]["name"];
    $extension = pathinfo($upload_file, PATHINFO_EXTENSION);
    if ($extension == 'csv') {
      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    } else if ($extension == 'xls') {
      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    } else {
      $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    }
    $spreadSheet = $reader->load($_FILES['file_excel']['tmp_name']);
    $sheetData = $spreadSheet->getActiveSheet()->toArray();
    // echo "<pre>";
    // print_r($sheetData);
    // echo "</pre>";
    $sheetCount = count($sheetData);
    /*  var_dump($sheetData);
      die(); */
    if ($sheetCount > 1) {
      $dataDosen = array();
      $dataUser = array();
      for ($i = 0; $i < $sheetCount; $i++) {
        $username = $sheetData[$i][10];
        $password = $sheetData[$i][11];
        $nama = $sheetData[$i][12];
        $email = $sheetData[$i][13];
        /* $id_group = $sheetData[$i][14];
          $is_active = $sheetData[$i][15];
          $class_modifite_user = $sheetData[$i][16]; */
        /* $class_modifite_time = $sheetData[$i][17]; */

        $dataUser[$i] = array(
          "username" => $username,
          "password" => $password,
          "nama" => $nama,
          "email" => $email,
          /* "id_group" => $id_group,
            "is_active" => $is_active,
            "class_modifite_user" => $class_modifite_user, */
          // "class_modifite_time" => $class_modifite_time,
        );
        /* var_dump($dataUser);
          die(); */

        $this->Admin_model->register("t_user", $dataUser[$i]);
        $id_user = $this->db->insert_id();

        // $id_dosen = $sheetData[$i][0];
        $nip = $sheetData[$i][1];
        $nama_d = $sheetData[$i][2];
        $prodi = $sheetData[$i][3];
        $jenis_kelamin = $sheetData[$i][4];
        $pendidikan_terakhir = $sheetData[$i][5];
        /* $id_user = $sheetData[$i][6]; */
        $is_active = $sheetData[$i][7];
        /* $class_modifite_user = $sheetData[$i][8]; */
        $class_modifite_time = $sheetData[$i][9];

        $dataDosen[$i] = array(
          "nip" => $nip,
          "nama_d" => $nama_d,
          "prodi" => $prodi,
          "jenis_kelamin" => $jenis_kelamin,
          "pendidikan_terakhir" => $pendidikan_terakhir,
          "id_user" => $id_user,
          /* "is_active" => $is_active, */
          /* "class_modifite_user" => $class_modifite_user, */
          /* "class_modifite_time" => $class_modifite_time, */
        );

        $this->Admin_model->register("dosen", $dataDosen[$i]);
      }
      $this->session->set_flashdata('sukses', 'Import data Dosen berasil masuk!');
      return redirect('Admin/dosen');
    }
    // echo "<pre>";
    // print_r($dataUser);
    // echo "</pre>";

  }


  public function get_admin()
  {
    $query = $this->Admin_model->admin();

    $data = [];

    foreach ($query as $p => $r) {

      $data[] = array(
        $p + 1,
        $r->nama,
        $r->nama_agama,
        $r->nama_prodi,
        $r->nim,
      );
    }

    $result = array(
      "data" => $data
    );

    echo json_encode($result);
    /* exit(); */
  }
  public function get_admin_dosen()
  {
    $query = $this->Admin_model->dosen();

    $data = [];

    foreach ($query as $p => $r) {

      $data[] = array(
        $p + 1,
        $r->nama_d,
        $r->nip,
        $r->prodi,
        $r->jenis_kelamin,
        $r->pendidikan_terakhir,
      );
    }

    $result = array(
      "data" => $data
    );

    echo json_encode($result);
  }

  public function tambah_dosen()
  {
    /* $this->form_validation->set_rules('nama_d', 'nama', 'trim|required|callback_checkAlphaOnly|min_length[6]'); */
    $this->form_validation->set_rules('nip', 'nomor induk pengajar', 'trim|required|is_natural|min_length[12]');
    $this->form_validation->set_rules('prodi', 'prodi', 'required');
    $this->form_validation->set_rules('email', 'prodi', 'required');
    $this->form_validation->set_rules('pendidikan_terakhir', 'prodi', 'required');
    $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'required');
    $this->form_validation->set_rules('password', 'username', 'required');
    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

    $this->form_validation->set_message('required', '{field} harus diisi!');
    $this->form_validation->set_message('checkAlphaOnly', '{field} harus di isi dengan huruf!');
    $this->form_validation->set_message('checkNomor', '{field} anda tidak sesuai, silahkan cek nomor telepone anda kembali');
    $this->form_validation->set_message('min_length', '{field} tidak mencukupi batas minimal!!');
    $this->form_validation->set_message('is_natural', '{field} harus berupa angka!!');
    $this->form_validation->set_message('matches', '{field} harus sama dengan password!!');
    $this->form_validation->set_message('valid_age', '{field} harus melebihi 16 tahun!!');

    if ($this->form_validation->run() === FALSE) {

      foreach ($this->input->post() as $key => $value) {
        $res[$key] = form_error($key, '<div class="text-danger">', '</div>');
      }
      echo json_encode(['success' => false, 'message' => $res]);
    } else {

      $user = array(
        'username' => $this->input->post('email'),
        'email' => $this->input->post('email'),
        'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
        'nama' => $this->input->post('nama'),
      );

      $this->db->insert('t_user', $user);
      $id_user = $this->db->insert_id();

      $data = array(
        'id_user' => $id_user,
        'nama_d' => $this->input->post('nama'),
        'nip' => $this->input->post('nip'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
        'prodi' => $this->input->post('prodi')
      );

      $saved = $this->db->insert('dosen', $data);

      if ($saved) {
        echo json_encode(['success' => true, 'message' => 'dosen berasil ditambah']);
      }
    }
  }
}
