<?php
defined('BASEPATH') or exit('No direct script access allowed');


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Mahasiswa extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Mahasiswa_model");
    $this->load->model("M_regis");
    if ($this->session->userdata('logged') != TRUE) {
      $url = base_url('login');
      redirect($url);
    };
  }

  public function index()
  {
    $data["title"] = "List Data Mahasiswa";
    $data["data_mahasiswa"] = $this->Mahasiswa_model->getAll();
    $data['dosen'] = $this->Mahasiswa_model->dosen()->result();
    $foto['mahasiswa'] = $this->M_regis->mahasiswa();
    $this->load->view('tpls/sidebar', $foto);
    $this->load->view('vw_mahasiswa', $data);
    $this->load->view('tpls/footer');
  }

  public function get_mahasiswa()
   {
      $query = $this->Mahasiswa_model->mhsw();
      
      $data = [];
      
      foreach($query as $p => $r) {
         if($r->status === '1'){
            $btn_verif = '<label class="badge badge-success">Disetujui</label>';
            $btn_print = '<a type="button" target="_blank" href="'.site_url('mahasiswa/pdf/'. $r->id).'" class="btn btn-lg btn-danger text-white me-0"><i class="mdi mdi-file-pdf"></i> Pdf</a><a type="button" target="_blank" href="'.site_url('Mahasiswa/excel').'" class="btn btn-lg btn-success text-white me-0"><i class="mdi mdi-file-import"></i> Excel</a>';
         }else{
            $btn_verif = '<label class="badge badge-warning">Menunggu</label>';
            $btn_print = '<h5 style="color:gray;">Belum Bisa <strong style="color:black;">Cetak !.<strong></h5>';
        }
           $data[] = array(
                $p+1,
                $r->nama,
                ''.$this->session->userdata('username').'',
                '<strong>'.$r->proposal.'</strong>',
                $r->nama_d,
                $btn_verif,
                $btn_print
           );
      }

      $result = array(
                 "data" => $data
            );
      echo json_encode($result);
   }

  public function pdf($id)
  {
    $this->load->model('mahasiswa_model');
    $data['mahasiswa'] = $this->Mahasiswa_model->getisi($id);
    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = "laporan-data-siswa.pdf";
    $this->pdf->load_view('laporan_siswa', $data);
  }

  public function excel()
  {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = [
      'font' => ['bold' => true], // Set font nya jadi bold
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = [
      'alignment' => [
        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ],
      'borders' => [
        'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
        'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
        'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
        'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
      ]
    ];
    $sheet->setCellValue('A1', "DATA MAHASISWA UNIVERSITAS SEBELAS MARET"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $sheet->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
    $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1
    // Buat header tabel nya pada baris ke 3
    $sheet->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $sheet->setCellValue('B3', "NIM"); // Set kolom B3 dengan tulisan "NIS"
    $sheet->setCellValue('C3', "NAMA"); // Set kolom C3 dengan tulisan "NAMA"
    $sheet->setCellValue('D3', "EMAIL"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $sheet->setCellValue('E3', "PRODI");
    $sheet->setCellValue('F3', "JUDUL"); // Set kolom E3 dengan tulisan "ALAMAT"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $sheet->getStyle('A3')->applyFromArray($style_col);
    $sheet->getStyle('B3')->applyFromArray($style_col);
    $sheet->getStyle('C3')->applyFromArray($style_col);
    $sheet->getStyle('D3')->applyFromArray($style_col);
    $sheet->getStyle('E3')->applyFromArray($style_col);
    $sheet->getStyle('F3')->applyFromArray($style_col);
    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
    $siswa = $this->Mahasiswa_model->getData();
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach ($siswa as $data) { // Lakukan looping pada variabel siswa
      $sheet->setCellValue('A' . $numrow, $no);
      $sheet->setCellValue('B' . $numrow, $data->nim);
      $sheet->setCellValue('C' . $numrow, $data->nama);
      $sheet->setCellValue('D' . $numrow, $data->email);
      $sheet->setCellValue('E' . $numrow, $data->nama_prodi);
      $sheet->setCellValue('F' . $numrow, $data->proposal);

      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $sheet->getStyle('A' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('B' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('C' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('D' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('E' . $numrow)->applyFromArray($style_row);
      $sheet->getStyle('F' . $numrow)->applyFromArray($style_row);

      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $sheet->getColumnDimension('E')->setWidth(30); // Set width kolom E
    $sheet->getColumnDimension('F')->setWidth(30);

    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $sheet->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $sheet->setTitle("Laporan Data Siswa");
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Mahasiswa.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');
  }

  public function add()
  {
    $Mahasiswa = $this->Mahasiswa_model; //objek model
    $this->form_validation->set_rules('proposal', 'proposal', 'required');
    $this->form_validation->set_rules('dosen', 'dosen', 'required');

    $this->form_validation->set_message('required', '{field} harus diisi!');
    $this->form_validation->set_message('is_unique', '{field} Sudah digunakan!');
    $this->form_validation->set_message('matches', '{field} harus sama dengan {param}!');

    if ($this->form_validation->run() === FALSE) {
      foreach ($this->input->post() as $key => $value) {
        $res[$key] = form_error($key, '<div class="text-danger">', '</div>');
      }
      echo json_encode(['success' => false, 'message' => $res]);
    } else {
      /*  $id = $this->Mahasiswa_model->mahasiswa() */
      $Mahasiswa->save(); //menerapkan rules validasi pada mahasiswa_model
      echo json_encode(['success' => true, 'message' => 'Judul proposal berhasil di tambah.']);
    }
  }

  public function edit($id = null)
  {
    if (!isset($id)) redirect('mahasiswa');

    $Mahasiswa = $this->Mahasiswa_model;
    $validation = $this->form_validation;
    $validation->set_rules($Mahasiswa->rules());
    $Mahasiswa->update($id);
    echo json_encode(['success' => true, 'message' => 'mahasiswa berasil diupdate']);
  }
  public function delete($id)
  {
    $deleted = $this->Mahasiswa_model->delete($id);

    if ($deleted) {
      echo json_encode(['success' => true, 'message' => 'mahasiswa berasil didelete']);
    }
  }
}
