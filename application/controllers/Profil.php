<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Profil extends CI_Controller {
	var $userData;
    function __construct()
	{
		parent::__construct();
		$this->load->model('M_regis');
        if($this->session->userdata('logged') !=TRUE){
            $url=base_url('login');
            redirect($url);
		};
	}

    public function index()
	{
        $data['agama'] = $this->M_regis->agama()->result();
        $data['prodi'] = $this->M_regis->prodi()->result();
        $data['provinces'] = $this->M_regis->provinsi()->result();
		$data['mahasiswa'] = $this->M_regis->mahasiswa();
		/* $data['user_group'] = $this->M_regis->grup()->result(); */
        $this->load->view('tpls/sidebar', $data);
		$this->load->view('tpls/vw_profile', $data);
		$this->load->view('tpls/footer');
	}

    public function edit_profil()
	{
		if ($this->form_validation->run()==false)
	   	{
			$data['agama'] = $this->M_regis->agama()->result();
        	$data['prodi'] = $this->M_regis->prodi()->result();
        	$data['provinces'] = $this->M_regis->provinsi()->result();
			$data['mahasiswa'] = $this->M_regis->mahasiswa();
			/* $data['user_group'] = $this->M_regis->grup()->result(); */
			if(isset($_POST['provinces'])){
				$data['regencies'] = $this->M_regis->kabupaten($_POST['provinces'])->result();		
			}
			$this->load->view('tpls/vw_profile', $data); 
		}
		else
		{
			
			
			/* $id_user = $this->M_regis->save(); */
			
			$user = array(
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
				'nama' => $this->input->post('nama'),
				/* 'id_group' => $this->input->post('id_group'), */
			);

			$this->db->edit_profil('t_user',$user, array('id' => $this->input->post('id')));
			$id_user = $this->db->insert_id();
			/* return $this->db->insert($this->table, $data); */

			$data = array(
				'id_user' => $id_user,
				'nama' => $this->input->post('nama'),
				'nim' => $this->input->post('nim'),
				'email' => $this->input->post('email'),
				'nomor' => $this->input->post('nomor'),
				'tempat' => $this->input->post('tempat'),
				'tanggal' => $this->input->post('tanggal'),
				'agama' => $this->input->post('agama'),
				'provinsi' => $this->input->post('provinsi'),
				'kabupaten' => $this->input->post('kabupaten'),
				'kecamatan' => $this->input->post('kecamatan'),
				'prodi' => $this->input->post('prodi'),
				'nik' => $this->input->post('nik'),
				'alamat' => $this->input->post('alamat'),
			);

			$edit = $this->db->edit_profil('mahasiswa',$data, array('IdMhsw' => $this->input->post('IdMhsw')));
			if ($edit) {
				$this->session->set_flashdata('message_success', 'Data mahasiswa berasil terdaftar!');
				return redirect('profil');
			}

		}
	}

	public function upload_avatar($id = null)
{
	if ($this->input->method() === 'post') {
		// the user id contain dot, so we must remove it
		$file_name = $_FILES['foto']['name'];
		$path = '/uploud/avatar/';
		$config['upload_path']          = FCPATH. $path;
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name']            = $file_name;
		$config['overwrite']            = true;

		$this->load->library('upload', $config);
		$id = $this->session->userdata('id');

		if (isset($_FILES['foto']['name']) && $_FILES['foto']['name'] != "") {
		if (!$this->upload->do_upload('foto')) {
			$data['error'] = $this->upload->display_errors();
			$uploaded_data['file_name'] = "";
		} else {
			$uploaded_data = $this->upload->data();
		}
			$finalFileName =  $path . $uploaded_data['file_name'];
			$checkOldFile = $this->M_regis->getAvatar($id)->row();

			if (file_exists(FCPATH . $checkOldFile->foto))
                unlink(FCPATH . $checkOldFile->foto);
            $foto = array('foto' => $finalFileName);
            $saved = $this->M_regis->updateData($id, $foto);


	
			if ($saved) {
				$this->session->set_flashdata('message_success', 'foto updated!',['foto' => $finalFileName]);
				redirect(site_url('profil'));
			}
		}
	}
}

	
}

 
