<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Dasboard extends CI_Controller {
 
	function __construct(){
		parent::__construct();
        $this->load->model("Mahasiswa_model");
		if($this->session->userdata('logged') !=TRUE){
            $url=base_url('login');
            redirect($url);
		};
	}
 
	public function index()
	{
		$this->load->view('tpls/sidebar');
		$this->load->view('vw_dasboard');
		$this->load->view('tpls/footer');
		
	}

	public function mahasiswa()
	{
		$this->load->view('tpls/sidebar');
		$this->load->view('vw_mahasiswa');
		$this->load->view('tpls/footer');

	}

	public function get_role()
   {
      $query = $this->Mahasiswa_model->group();
      
      $data = [];
      
      foreach($query as $p => $r) {
           $data[] = array(
                $p+1,
                $r->nama_group,
                '<a type="button" target="_blank" href="" class="btn btn-lg btn-primary text-white me-0"><i class="mdi mdi-lead-pencil"></i> Edit</a>',
           );
      }

      $result = array(
                 "data" => $data
            );
      echo json_encode($result);
   }
}