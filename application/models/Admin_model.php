<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    private $table = 'mahasiswa';

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["IdMhsw" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("IdMhsw", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    public function admin(){
       /*  $id = $this->session->userdata('id'); */
        $this->db->select('m.nama,a.nama_agama, p.nama_prodi,m.nim');
        $this->db->from('mahasiswa as m');
        $query= $this->db->join('agama as a ', 'a.id = m.agama');
        $query= $this->db->join('prodi as p ', 'p.id = m.prodi');
        $query= $this->db->order_by("m.IdMhsw");
        return $query->get()->result();
    }
    public function dosen(){
        /*  $id = $this->session->userdata('id'); */
         $this->db->select('d.nama_d,d.nip, d.jenis_kelamin, d.prodi, d.pendidikan_terakhir');
         $this->db->from('dosen as d');
         $query= $this->db->order_by("d.id");
         return $query->get()->result();
    }

    public function register($table, $data)
	{
		return $this->db->insert($table, $data);
	}

}
