<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_regis extends CI_Model{
     
    private $table = 'mahasiswa';
    public function __construct()
	{
        parent::__construct();
	}

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["IdMhsw" => $id])->row();
        //query diatas seperti halnya query pada mysql 
        //select * from mahasiswa where IdMhsw='$id'
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("IdMhsw", "desc");
        $query = $this->db->get();
        return $query;
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    function agama(){
        $query = $this->db->get('agama');
        return $query;  
    }

    function prodi(){
        $query = $this->db->get('prodi');
        return $query;  
    }

    function provinsi(){
        $query = $this->db->get('provinces');
        return $query;  
    }

    function grup(){
        $query = $this->db->get('user_group');
        return $query;  
    }

    function kabupaten($provinces_id){
        $query = $this->db->get_where('regencies', array('province_id' => $provinces_id));
        return $query;
    }

    function kecamatan($regencies_id){
        $query = $this->db->get_where('districts', array('regency_id' => $regencies_id));
        return $query;
    }

    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "nik" => $this->input->post('nik'),
            "nim" => $this->input->post('nim'),
            "agama" => $this->input->post('agama'),
            "tempat" => $this->input->post('tempat'),
            "tanggal" => $this->input->post('tanggal'),
            "nomor" => $this->input->post('nomor'),
            "alamat" => $this->input->post('alamat'),
            "provinsi" => $this->input->post('provinsi'),
            "kabupaten" => $this->input->post('kabupaten'),
            "kecamatan" => $this->input->post('kecamatan'),
            "prodi" => $this->input->post('prodi'),
            "id_user" =>$this->db->insert_id('t_user')
        );
        return $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function mahasiswa(){
        $id = $this->session->userdata('id');
        $this->db->select('m.nama, m.nik, m.nim, m.tempat, m.nomor,m.provinsi, m.kabupaten, m.email, m.agama, m.tanggal, m.alamat, m.foto, t.username, t.password');
        $this->db->from('mahasiswa as m');
        $query= $this->db->join('agama as a ', 'a.id = m.agama');
        $query= $this->db->join('prodi as p ', 'p.id = m.prodi');
        $query= $this->db->join('t_user as t', 't.id = m.id_user');
        $query= $this->db->where(array('m.id_user'=> $id));
        return $query->get()->result();
    }

    public function updateData($id, $data)
    {
        return $this->db->update($this->table, $data, ['id_user'=>$id]);
    }

    public function getAvatar($idUser)
    {
        $this->db->select('foto');
        $this->db->from('mahasiswa');
        $this->db->where('id_user', $idUser);
        return $this->db->get();
    }

    public function update($id)
    {
        $data = array(
            "foto" => $this->input->post('foto')
        );
        return $this->db->update($this->table, $data, array('id' => $id));
    }


    
     
}